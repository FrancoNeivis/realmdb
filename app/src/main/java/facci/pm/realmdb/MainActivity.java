package facci.pm.realmdb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    Realm uiThreadRealm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Realm.init(this); // context, usually an Activity or Application

        //Open a Realm
        String realmName = "My Project";
        RealmConfiguration config = new RealmConfiguration.Builder().name(realmName).build();
        uiThreadRealm = Realm.getInstance(config);

        addChangeListenerToRealm(uiThreadRealm);

        FutureTask<String> Product = new FutureTask(new BackgroundQuickStart(), "test");
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.execute(Product);
    }
    private void addChangeListenerToRealm(Realm realm) {
        // all Tasks in the realm
        RealmResults<Product> Products = uiThreadRealm.where(Product.class).findAllAsync();
        Products.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<Product>>() {
            @Override
            public void onChange(RealmResults<Product> products, OrderedCollectionChangeSet changeSet) {
                // process deletions in reverse order if maintaining parallel data structures so indices don't change as you iterate

                OrderedCollectionChangeSet.Range[] deletions = changeSet.getDeletionRanges();
                for (OrderedCollectionChangeSet.Range range : deletions) {
                    Log.e("QUICKSTART", "Deleted range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
                }

                OrderedCollectionChangeSet.Range[] insertions = changeSet.getInsertionRanges();
                for (OrderedCollectionChangeSet.Range range : insertions) {
                    Log.e("QUICKSTART", "Inserted range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
                }

                OrderedCollectionChangeSet.Range[] modifications = changeSet.getChangeRanges();
                for (OrderedCollectionChangeSet.Range range : modifications) {
                    Log.e("QUICKSTART", "Updated range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // the ui thread realm uses asynchronous transactions, so we can only safely close the realm
        // when the activity ends and we can safely assume that those transactions have completed
        uiThreadRealm.close();
    }

    public class BackgroundQuickStart implements Runnable {
        private Product otherProduct;

        @Override
        public void run() {
            String realmName = "My Project";
            RealmConfiguration config = new RealmConfiguration.Builder().name(realmName).build();
            Realm backgroundThreadRealm = Realm.getInstance(config);

            //To create a new Task, instantiate an instance of the Task class and add it to the realm in a write block
            Product Product1 = new Product("Papas Francesas", "Papas sabrosas con salsa rosada", "2", "50", "Postres");
            Product Product2 = new Product("Mousse de Fresa", "postre de yougurt con trocitos de fresa ", "3", "20", "Papas");

            backgroundThreadRealm.executeTransaction (transactionRealm -> {
                transactionRealm.insert(Product1);
                transactionRealm.insert(Product2);
            });

            //----Metodo Modificar------
            //You can retrieve a live collection of all items in the realm
            // all Tasks in the realm
            RealmResults<Product> Products = backgroundThreadRealm.where(Product.class).findAll();

            //To MODIFY a task, update its properties in a write transaction block
            Product otherTask = Products.get(0);
//           // all modifications to a realm must happen inside of a write block
            backgroundThreadRealm.executeTransaction( transactionRealm -> {
                Product innerOtherProduct = transactionRealm.where(Product.class).equalTo("name", otherProduct.getNombre()).findFirst();
             innerOtherProduct.setStatus(ProductStatus.Open);
           });



            //you can DELETE a task by calling the deleteFromRealm() method in a write transaction block:
            Product yetAnotherProduct = Products.get(0);
            String yetAnotherProductNombre = yetAnotherProduct.getNombre();
            String yetAnotherProductDescripcion = yetAnotherProduct.getDescripcion();
            String yetAnotherProductPrecio = yetAnotherProduct.getPrecio();
            String yetAnotherProductStock = yetAnotherProduct.getStock();
            String yetAnotherProductCategoria = yetAnotherProduct.getCategoria();

//            all modifications to a realm must happen inside of a write block

            backgroundThreadRealm.executeTransaction( transactionRealm -> {
                Product innerYetAnotherProduct = transactionRealm.where(Product.class)
                        .equalTo("_id", yetAnotherProductNombre)
                        .equalTo("_id", yetAnotherProductDescripcion)
                        .equalTo("_id", yetAnotherProductPrecio)
                        .equalTo("_id", yetAnotherProductStock)
                        .equalTo("_id", yetAnotherProductCategoria)
                        .findFirst();
                innerYetAnotherProduct.deleteFromRealm();
            });



            for (Product product: Products
            ) {
                Log.e("Product", "Name: " + product.getNombre()
                        +" Status: " + product.getStatus()
                        +" Descripcion: " + product.getDescripcion()
                        +" Precio: " + product.getPrecio()
                        +" Stock: " + product.getStock()
                        +" Categoria: " + product.getCategoria()
                );
            }


            // you can also filter a collection
            RealmResults<Product> ProductsThatBeginWithN = Products.where().beginsWith("name", "N").findAll();
            RealmResults<Product> openProducts = Products.where().equalTo("status", ProductStatus.Open.name()).findAll();



            // because this background thread uses synchronous realm transactions, at this point all
            // transactions have completed and we can safely close the realm
            backgroundThreadRealm.close();
        }
    }
}