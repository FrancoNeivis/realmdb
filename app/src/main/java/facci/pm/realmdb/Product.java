package facci.pm.realmdb;

import io.realm.RealmObject;
import io.realm.annotations.Required;

public class Product extends RealmObject {

    @Required
    private String nombre;
    @Required
    private String status = TaskStatus.Open.name();
    @Required
    private String descripcion;
    @Required
    private String precio;
    @Required
    private String stock;
    @Required
    private String categoria;

    public Product(String _nombre, String _descripcion, String _precio, String _stock, String _categoria) {

        this.nombre = _nombre;
        this.descripcion = _descripcion;
        this.precio = _precio;
        this.stock = _stock;
        this.categoria = _categoria;

    }
    public Product(String s){

    }

    public void setStatus( ProductStatus status) { this.status = status.name();}
    public String getStatus() { return this.status; }

    public String getNombre() {return  nombre;}
    public void setNombre(String nombre) { this.nombre = nombre;}

    public String getDescripcion() { return descripcion; }
    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }

    public String getPrecio() { return precio; }
    public void setPrecio(String precio) { this.precio = precio; }

    public String getStock() { return stock; }
    public void setStock(String stock) { this.stock = stock; }

    public String getCategoria() { return categoria; }
    public void setCategoria(String categoria) { this.categoria = categoria; }
}
